let strength = 5;
let endurance = 5;
let moves = 0;
const maxMoves = 7;

document.getElementById('strengthEnduranceSlider').addEventListener('input', function(event) {
    const value = event.target.value;
    strength = value;
    endurance = 10 - value;
    document.getElementById('strengthValue').textContent = strength;
    document.getElementById('enduranceValue').textContent = endurance;
});

function validateValues() {
    document.getElementById('arrows').style.display = 'flex';
}

function moveSantaUp() {
    if (endurance <= 5 && moves >= maxMoves) {
        showMessage("Je n'ai pas assez d'endurance.");
        return;
    }
    const santa = document.getElementById('santa');
    const scene = document.getElementById('scene');
    const sceneRect = scene.getBoundingClientRect();
    const santaRect = santa.getBoundingClientRect();

    let newTop = parseInt(window.getComputedStyle(santa).top, 10);
    newTop -= 100;

    // Vérifier les limites
    if (newTop < 0) newTop = 0;

    santa.style.top = newTop + 'px';
    moves++;
    checkCollision();
    checkDelivery();
}

function moveSantaDown() {
    if (endurance <= 5 && moves >= maxMoves) {
        showMessage("Je n'ai pas assez d'endurance.");
        return;
    }
    const santa = document.getElementById('santa');
    const scene = document.getElementById('scene');
    const sceneRect = scene.getBoundingClientRect();
    const santaRect = santa.getBoundingClientRect();

    let newTop = parseInt(window.getComputedStyle(santa).top, 10);
    newTop += 100;

    // Vérifier les limites
    if (newTop + santaRect.height > sceneRect.height) newTop = sceneRect.height - santaRect.height;

    santa.style.top = newTop + 'px';
    moves++;
    checkCollision();
    checkDelivery();
}

function moveSantaLeft() {
    if (endurance <= 5 && moves >= maxMoves) {
        showMessage("Je n'ai pas assez d'endurance.");
        return;
    }
    const santa = document.getElementById('santa');
    const scene = document.getElementById('scene');
    const sceneRect = scene.getBoundingClientRect();
    const santaRect = santa.getBoundingClientRect();

    let newLeft = parseInt(window.getComputedStyle(santa).left, 10);
    newLeft -= 100;

    // Vérifier les limites
    if (newLeft < 0) newLeft = 0;

    santa.style.left = newLeft + 'px';
    moves++;
    checkCollision();
    checkDelivery();
}

function moveSantaRight() {
    if (endurance <= 5 && moves >= maxMoves) {
        showMessage("Je n'ai pas assez d'endurance.");
        return;
    }
    const santa = document.getElementById('santa');
    const scene = document.getElementById('scene');
    const sceneRect = scene.getBoundingClientRect();
    const santaRect = santa.getBoundingClientRect();

    let newLeft = parseInt(window.getComputedStyle(santa).left, 10);
    newLeft += 100;

    // Vérifier les limites
    if (newLeft + santaRect.width > sceneRect.width) newLeft = sceneRect.width - santaRect.width;

    santa.style.left = newLeft + 'px';
    moves++;
    checkCollision();
    checkDelivery();
}

function checkCollision() {
    const santa = document.getElementById('santa');
    const smallGift = document.getElementById('smallGift');
    const santaRect = santa.getBoundingClientRect();
    const smallGiftRect = smallGift.getBoundingClientRect();

    if (santaRect.right > smallGiftRect.left &&
        santaRect.left < smallGiftRect.right &&
        santaRect.bottom > smallGiftRect.top &&
        santaRect.top < smallGiftRect.bottom) {
        document.getElementById('giftChoice').style.display = 'block';
    } else {
        document.getElementById('giftChoice').style.display = 'none';
        document.getElementById('message').style.display = 'none';
    }
}

function chooseGift(size) {
    const message = document.getElementById('message');
    if (size === 'large' && strength <= 5) {
        message.textContent = "Impossible, je n'ai pas assez de force.";
        message.style.display = 'block';
    } else {
        message.style.display = 'none';
        if (size === 'large') {
            document.getElementById('largeGift').style.display = 'none';
        } else {
            document.getElementById('smallGift').style.display = 'none';
        }
        document.getElementById('giftChoice').style.display = 'none';
    }
}

function checkDelivery() {
    const santa = document.getElementById('santa');
    const tree = document.getElementById('tree');
    const santaRect = santa.getBoundingClientRect();
    const treeRect = tree.getBoundingClientRect();

    if (santaRect.right > treeRect.left &&
        santaRect.left < treeRect.right &&
        santaRect.bottom > treeRect.top &&
        santaRect.top < treeRect.bottom) {
        document.getElementById('deliveryMessage').style.display = 'block';
    }
}

function showMessage(text) {
    const message = document.getElementById('message');
    message.textContent = text;
    message.style.display = 'block';
}
