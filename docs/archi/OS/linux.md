# Apprendre la ligne de commande avec Linux


## Représentation de l'arborescence
On représente généralement l'arborescence de linux sous cette forme :
![arborescence](../img/exemple_arbo.png)
Les dossiers sont encadrés et les fichiers sont écrit sans encadrement.
Ici `sue` est le dossier parent de `Pictures` et le dossier `family` contient les fichers `mom.jpg `et `timmy.jpg`

## Chemin absolu
Le chemin absolu est le chemin que l'on doit emprunter **à partir de la racine** de la machine pour accéder au fichier. Il est toujours le même tant que l'on ne déplace pas le fichier où que ne le change pas de machine.
!!! example "Chemin de `timmy.jpg`"
	our accéder à ce fichier à partir de la racine du PC il faudra saisir : `/home/sue/Pictures/family/timmy.jpg`

## Chemin relatif
Le chemin relatif est le chemin où l'on peut trouver un ficher à partir du réoertoire courant (le répertoire où l'on se trouve). Cette méthode est plus souple si on déplace u projet en entier sur une clé US par exemple.

!!! example "Chemin relatif `timmy.jpg` à partir du répertoire `sue`""
	A partir du répertoire `sue`, on trouve le fichier `timmy.jpg` en saisissant : `Pictures/family/timmy.jpg`


!!! warning "Remonter d'un niveau `../`"

	Imaginons maintenant que nous désirions indiquer le chemin relatif pour accéder au fichier `timmy.jpg` depuis le répertoire `Music`. Comment faire ?  
	 Il faut "remonter" d'un "niveau" dans l'arborescence pour se retrouver dans le répertoire `sue et ainsi pouvoir repartir vers la bonne "branche". Pour ce faire il faut utiliser 2 points : `../Pictures/family/timmy.jpg`

	Il est tout à fait possible de remonter de plusieurs niveaux dans l'arborescence : `../../` depuis le répertoire `Docs` permet de remonter dans le répertoire `home` : `../../sue/Pictures/family/timmy.jpg`

!!! info "Le répertoire `home`"

	Comme déjà évoqué plus haut, les systèmes de type "UNIX" sont des systèmes"multi-utilisateurs": chaque utilisateur possède son propre compte. Chaque utilisateur possède un répertoire à son nom, ces répertoires personnels se situent traditionnellement dans le répertoire `home`.  
	Dans l'arborescence ci-dessus, nous avons 2 utilisateurs : "sue" et "fred". Par défaut, quand un utilisateur ouvre une console, il se trouve dans son répertoire personnel : `/home/sue` par exemple.

	👉 Ce répertoire `home` de l'utilisateur est noté `~`. Sue peut, de tout répertoire ou elle se trouve, accéder à son fichier `~/picture/hawaii/img01.jpg`

## Prise en main et login

Pour explorer le monde de Linux, nous allons utiliser un émulateur du système Linux qui nous donnera l’accès à une invite de
commande [Emulateur Linux](https://chinginfo.fr/chapitre/dossier/weblinux){ .md-button target="_blank" rel="noopener" }
Lors du lancement, vous aurez un écran d’accueil de la forme suivante :

![nom image](../img/invite.png){ width=80% }

Les différents utilisateurs et mot de passe pour la connexion **sont inscrits en haut de la page**.

Nous les rappelons ci-dessous :

|Utilisateur|alice|bob|eve|root|user|
|:--:|:--:|:--:|:--:|:--:|:--:|
| Mot de passe|alice22|bob2022|2eve2|admin2022|22user|

Pour nous identifier avec l’utilisateur alice :

* d’abord saisir le login alice
* valider le choix en appuyant sur <kbd>Entrée</kbd>
* saisir le mot de passe alice22 (attention rien n’apparait à l’écran lors de la saisie du mot de passe)
* valider la saisie en appuyant sur <kbd>Entrée</kbd>

👉 Nous voyons que l’authentification a réussi en observant la ligne `alice:~$ `

!!! info "`alice:~$ `"

	* alice : indique le nom de l’utilisateur courant
	* ~ : indique le répertoire courant. Ici “~” indique le répertoire personnel de l’utilisateur courant (ici, alice).
	* $ : permet de séparer le préambule, des instructions saisies par l’utilisateur.

## Les commandes de bases

Voici une liste non exhaustive des commandes de linux.   

!!! note 
	Le réperoire courant est le répertoire dans lequel on se trouve.


### ls
!!! quote 
	liste les éléments contenu dans le répertoire courant.

![ls](../img/ls.png)

### pwd
!!! quote 
	Affiche le nom du répertoire courant.

![pwd](../img/pwd.png)

### whoami
!!! quote 
	Qui sui-je ?Affiche le nom de l'utilisateur connecté.

![whoami](../img/whoami.png)

### cd
!!! quote 
	Change directory. Permet de changer de répertoire en indiquant le chemin. Pour remonter dans le dossier parent on utilisera `../`

![cd](../img/cd.png)

### mkdir
!!! quote 
	Make directory. Création d'un répertoire dans le répertoire courant. Il faudra préciser le nom ou le chemin du dossier créé.

![mkdir](../img/mkdir.png)


### touch
!!! quote 
	Crée un fichier dont le nom est spécifié en paramètre de la commande

![touch](../img/touch.png)


### cp
!!! quote 
	Copy. Copie un fichier dans un autre répertoire ou dans le même. Le premier paramètre est le chemin du fichier à copier et le second est le chemin de la copie.

![cp](../img/cp.png)

### mv
!!! quote
	Move. Déplace un fichier. On precise d'abord le fichier à déplacer puis le chemin de la cible. Si on déplace un fichier dans le même répertoire, cela revient à le renommer.

![mv](../img/mv.png)

### rm
!!! quote
	Remove. Supprime un fichier ou un dossier

![rm](../img/rm.png)

!!! warning
	Linux ne demande pas si on est sur de vouloir supprimer. Attention à  l'utilisation de cette commande.

---
